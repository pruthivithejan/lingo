interface Props {
  children: React.ReactNode;
}

const FeedWrapper: React.FC<Props> = ({ children }: Props) => {
  return <div className="flex-1 relative top-0 pb-10">{children}</div>;
};

export default FeedWrapper;
