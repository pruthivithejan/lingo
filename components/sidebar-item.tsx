"use client";

import { Button } from "@/components/ui/button";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";

type Props = {
  label: string;
  iconSrc: string;
  href: string;
};

export const SidebarItem = ({ label, iconSrc, href }: Props) => {
  const pathname = usePathname();
  const active = pathname === href;
  return (
    <div>
      <Button
        variant={active ? "sidebarOutline" : "sidebar"}
        className="justify-start h-[52] w-full"
        asChild
      >
        <Link href={href}>
          <Image
            src={iconSrc}
            alt={label}
            className="mr-5"
            height={24}
            width={24}
          />
          {label}
        </Link>
      </Button>
    </div>
  );
};
