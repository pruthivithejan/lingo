interface Props {
  children: React.ReactNode;
}

const StickyWrapper: React.FC<Props> = ({ children }: Props) => {
  // Implement your component logic here

  return (
    <div className="hidden lg:block w-[356px] sticky self-end bottom-6 ">
      <div className="min-h-[calc(100vh-48px)] sticky top-6 flex flex-col gap-y-4">
        {children}
      </div>
    </div>
  );
};

export default StickyWrapper;
