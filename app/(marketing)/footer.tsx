import { Button } from "@/components/ui/button";
import Image from "next/image";

export const Footer = () => {
  const countries = [
    { name: "Croatain", src: "hr.svg" },
    { name: "Spanish", src: "es.svg" },
    { name: "French", src: "fr.svg" },
    { name: "Italian", src: "it.svg" },
    { name: "Japanese", src: "jp.svg" },
  ];

  return (
    <footer className="hidden lg:block h-20 w-full border-slate-200 border-t-2 p-2">
      <div className="max-w-screen-lg mx-auto flex items-center justify-evenly h-full">
        {countries.map((country) => (
          <Button
            size="lg"
            variant="ghost"
            className="w-full"
            key={country.name}
          >
            <Image
              className="mr-4 rounded-md"
              alt={country.name}
              src={country.src}
              height={32}
              width={40}
            />
            {country.name}
          </Button>
        ))}
      </div>
    </footer>
  );
};
