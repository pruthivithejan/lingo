import { cn } from "@/lib/utils";
import { Check } from "lucide-react";
import Image from "next/image";

interface CardProps {
  title: string;
  id: number;
  imgSrc: string;
  disabled?: boolean;
  active?: boolean;
  onClick: (id: number) => void;
}

const Card: React.FC<CardProps> = ({
  title,
  imgSrc,
  id,
  active,
  disabled,
  onClick,
}) => {
  return (
    <div
      onClick={() => onClick(id)}
      className={cn(
        "h-full border-2 rounded-xl border-b-4 hover:bg-black/5 cursor-pointer active:border-b-2 flex flex-col items-center justify-between p-3 pb-6 min-h-[217px] min-w-[200px]",
        disabled && "opacity-50 pointer-events-none"
      )}
    >
      <div className="min-h-[24px] items-center justify-end w-full flex">
        {active && (
          <div className="rounded-md bg-green-600 flex items-center justify-center p-1.5">
            <Check className="text-white stroke-[4] h-4 w-4" />
          </div>
        )}
      </div>
      <Image
        alt={title}
        src={imgSrc}
        height={70}
        width={93.33}
        className="rounded-lg drop-shadow-md border object-cover"
      />
      <p className="text-neutral-700 text-center font-bold mt-3">{title}</p>
    </div>
  );
};

export default Card;
