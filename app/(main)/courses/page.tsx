import { getCourses, getUserProgress } from "@/database/queries";
import { List } from "./list";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Lingo | Courses",
  description: "A simple app to learn languages",
};

const CoursesPage = async () => {
  const coursesData = getCourses();
  const userProgressData = getUserProgress();

  const [courses, userProgress] = await Promise.all([
    coursesData,
    userProgressData,
  ]);

  return (
    <div className="h-full max-w-[912px] px-3 mx-auto">
      <h1 className="text-2xl font-bold text-neutral-700">Language Courses</h1>
      <List activeCourseId={userProgress?.activeCourseId} courses={courses} />
    </div>
  );
};

export default CoursesPage;
