# Lingo - Dualingo Clone

## by [Code with Antonio](https://www.youtube.com/watch?v=dP75Khfy4s4&t=16328s)

### Tech Stack

- Next.js 14
- React 18
- Tailwind CSS
- Shadcn UI
- Drizzle ORM
- Clerk Authentication
- PostgreSQL by Neon

### Links

- Clerk: https://go.clerk.com/wmPbEeD
- Kenney Assets:https://kenney.nl/
- Freesound: https://freesound.org/
- Elevenlabs AI: https://elevenlabs.io/
- Flagpack: https://flagpack.xyz/
- Icons: https://iconduck.com/licenses/mit

### Key Features:

- 🌐 Next.js 14 & server actions
- 🗣 AI Voices using Elevenlabs AI
- 🎨 Beautiful component system using Shadcn UI
- 🎭 Amazing characters thanks to KenneyNL
- 🔐 Auth using Clerk
- 🔊 Sound effects
- ❤️ Hearts system
- 🌟 Points / XP system
- 💔 No hearts left popup
- 🚪 Exit confirmation popup
- 🔄 Practice old lessons to regain hearts
- 🏆 Leaderboard
- 🗺 Quests milestones
- 🛍 Shop system to exchange points with hearts
- 💳 Pro tier for unlimited hearts using Stripe
- 🏠 Landing page
- 📊 Admin dashboard React Admin
- 🌧 ORM using DrizzleORM
- 💾 PostgresDB using NeonDB
- 🚀 Deployment on Vercel
- 📱 Mobile responsiveness

#

- Timestamp [04:45:15](https://www.youtube.com/watch?v=dP75Khfy4s4&t=17115s) Footer
